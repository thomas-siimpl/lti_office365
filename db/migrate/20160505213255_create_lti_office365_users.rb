class CreateLtiOffice365Users < ActiveRecord::Migration
  def change
    create_table :lti_office365_users do |t|
      t.string :name
      t.string :email
      t.string :uid, null: false
      t.string :token
      t.string :refresh_token

      t.timestamps null: false
    end
    add_index :lti_office365_users, :uid, unique: true
  end
end
