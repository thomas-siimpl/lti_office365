$LOAD_PATH.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'lti_office365/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'lti_office365'
  s.version     = LtiOffice365::VERSION
  s.authors     = ['Thomas Mulloy']
  s.email       = ['thomas@siimpl.io']
  s.homepage    = 'http://siimpl.io'
  s.summary     = 'Summary of LtiOffice365.'
  s.description = 'Description of LtiOffice365.'
  s.license     = 'MIT'

  s.files = Dir[
    '{app,config,db,lib}/**/*',
    'MIT-LICENSE',
    'Rakefile',
    'README.rdoc',
    'README.md'
  ]
  s.test_files = Dir['test/**/*']

  s.add_dependency 'rails', '~> 4.2', '>= 4.2.6'
  s.add_dependency 'ims-lti', '~> 2.0.0.beta'
  s.add_dependency 'httparty', '~> 0.13.7'
  s.add_dependency 'omniauth-oauth2', '1.1.2' # Fix for `callback_url`
  s.add_dependency 'omniauth-azure-oauth2', '~> 0.0.6'

  s.add_development_dependency 'sqlite3'
  s.add_development_dependency 'webmock'
end
