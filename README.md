# LtiOffice365

## Recommended Requirements
- Ruby 2.3.0

## Rails
LtiOffice365 RubyGem is designed for Rails and is a [Rails Engine](http://guides.rubyonrails.org/engines.html).
The following setup procedure assumes you have an existing Rails application and is in the context of your Rails
application. Please refer to the [Getting Started with Rails](http://guides.rubyonrails.org/getting_started.html)
guide if you have yet to create a Rails application.

### Setup Existing Rails Application
Include [LtiOffice365](https://bitbucket.org/thomas-siimpl/lti_office365) in `Gemfile`.
```
gem 'lti_office365'
```

Install
```
$ bundle

# Copy migration for LtiOffice365::User
$ rake lti_office365:install:migrations

# Inspect the migration in `db/migrate` and note the columns.
# If everything looks good with the User namespace then run the migration
$ rake db:migrate
```

Mount the engine in `config/routes.rb`.
```
mount LtiOffice365::Engine => '/'
```

### Example Rails Application
Since this is a [Rails Engine](http://guides.rubyonrails.org/engines.html), Rails provides a nice "Dummy" application
located at `test/dummy` with this engine setup like above.

```
If you want to run any commands that need to be run in context
of the application, like `rails server` or `rails console`,
you should do it from application's directory (typically test/dummy).
```

### LTI Provider Routes
If you're using this gem as a mounted Rails Engine, you are provided a few helpful
sample controllers and routes for your Rails application. Use `$ rake routes` to
see those routes from your application directory or from this gem repository's
`test/dummy`.

```
Prefix Verb URI Pattern Controller#Action
lti_office365      /           LtiOffice365::Engine

Routes for LtiOffice365::Engine:
launch    GET|POST   /launch(.:format)                  lti_office365/launch#show
configure GET        /configure(.:format)               lti_office365/configure#show
directory GET        /files(/:id)(.:format)             lti_office365/files#index
download  GET        /files/download/:id(.:format)      lti_office365/files#download
          GET        /auth/:provider/callback(.:format) lti_office365/sessions#create
logout    GET|DELETE /logout(.:format)                  lti_office365/sessions#destroy
```

#### Launch
`/launch` a basic route for your LTI Provider launch that registers the LTI Consumer POST
parameters to the standard Rails `params` hash. The LTI Consumer POST parameters
are available within `params[:message]`. This route also will kickoff the Office365
Oauth Login. One thing to note is that your LTI Provider should be running behind SSL.
For development, see [below](## SSL (Development)).

#### XML Configuration
`/configure.xml` renders the XML Configuration for use with a LMS. To render
your own launch configuration xml create `config/launch.yml` with the following
environment namespace.

```
# See `attr_reader` and `attr_accessor` for available attributes
# https://github.com/instructure/ims-lti/blob/2.0.x/lib/ims/lti/services/tool_config.rb
default: &default
  title: LTI Provider
  # description: ''
  launch_url: https://localhost:3000/launch
  # icon: ''
  # secure_launch_url: ''
  # secure_icon: ''
  # cartridge_bundle: ''
  # cartridge_icon: ''
  # vendor_code: ''
  # vendor_name: ''
  # vendor_description: ''
  # vendor_url: ''
  # vendor_contact_email: ''
  # vendor_contact_name: ''
  # custom_params: {}
  # extensions: {}

development:
  <<: *default

test:
  <<: *default

production:
  <<: *default
```

This launch config is made available within `Rails.application.config.office365[:launch]` and
can be used in a controller action.

```
def show
  respond_to do |format|
    format.xml do
      render xml: Rails.application.config.office365[:launch].to_xml
    end
  end
end
```

#### OneDrive Directory
`/files` provides a basic list of folders and files contained in your OneDrive with basic
traverse, download and "Open in Office365" example functionality. Clicking on a `file` name
will create a share link with the OneDrive client and invoke download of the file. Clicking
on a `folder` will traverse into the directory.

## Without Rails
Though using as a mounted Rails Engine is recommended, `LtiOffice365` can still be used however
sans routes, controllers and other Rails goodies.
```
require 'office365'
```

## Basic Usage
`Office365::Client` provides very common action verb methods. The general flow for the
`Client` is initialize the class with your `User(token:string, refresh_token:string)` model, construct an endpoint
and send the request needed. The `Office365::Client` will construct the necessary `Authorization`
headers and update the `User.refresh_token` to make repeated requests.

```
# Initialize Client with User model
client = Office365::Client.new(user)

# Construct an API endpoint and options hash
endpoint = 'https://graph.microsoft.com/endpoint'
options = {}

# Perform action (get|post|put|delete)
client.get(endpoint, options)
```

`Office365::OnedriveService` allows you to make OneDrive specific requests, such as `create_share_link`.
```
# Initialize OneDrive Service
service = Office365::OnedriveService.new(user)

# Create share link for OneDrive item
url = service.create_share_link(params[:id])
```

## Environment
Office365 requires the following `ENV` constants:
```
OFFICE365_CLIENT_ID=client_id
OFFICE365_CLIENT_SECRET=client_secret
```

`OFFICE365_CLIENT_ID` and `OFFICE365_CLIENT_SECRET` are supplied after [Adding an Application](https://azure.microsoft.com/en-us/documentation/articles/active-directory-integrating-applications) to [Azure Active Directory](https://manage.windowsazure.com).
Within Azure AD, those values may be referred to as `CLIENT ID` (`OFFICE365_CLIENT_ID`) and `KEY` (`OFFICE365_CLIENT_SECRET`).

## Environment (Development)
A convenient way to load `ENV` constants into your application running locally is to
install [foreman](https://github.com/ddollar/foreman).
```
$ gem install foreman
```

Then create a `.env` file and `Procfile` at the root of your application folder.
Populate the `.env` with the above `ENV` and populate the `Procfile` with
```
web: rails s
```

Now you can simply run `$ foreman s` instead of `$ rails server`, however now your `.env`
will be loaded. To load `.env` with `foreman` in the scope of other things such as
`$ rails console`, run `$ foreman run rails console` instead.

Never commit your `.env` file, be sure to include it in your `.gitignore`.

## SSL (Development)
A quick way to run your application with `https://` in development running on your
local machine is to download [ngrok](https://ngrok.com) and while running your local
web server `$ ngrok http <PORT>`.


## Linting
[RuboCop](https://github.com/bbatsov/rubocop) was used for Ruby linting with the following `.rubocop.yml`.

```
Rails:
  Enabled: true
Documentation:
  Enabled: false
Metrics/LineLength:
  Max: 120
AllCops:
  Exclude:
    - '**/schema.rb'
    - '**/db/migrate/*'
```

## Testing
```
# Run tests
$ rake test
```

### Helpful Resources
- [LTI Tool Provider Example](https://lti-tool-provider-example.herokuapp.com/xml_builder)
