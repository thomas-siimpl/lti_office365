require_dependency 'lti_office365/application_controller'

module LtiOffice365
  class FilesController < ApplicationController
    before_action :allow_iframe

    # Lists User's file at a `diectory_id`, otherwise root directory
    def index
      @directory = office365_client.get(Office365.directory(params[:id]))
    end

    # Creates OneDrive share link
    def download
      @share_link = office365_onedrive_service.create_share_link(params[:id])
      respond_to do |format|
        format.html { redirect_to @share_link }
        format.json { render json: { url: @share_link } }
      end
    end

    protected

    def office365_client
      @office365_client ||= Office365::Client.new(current_user)
    end

    def office365_onedrive_service
      @office365_onedrive_service ||= Office365::OnedriveService.new(current_user)
    end
  end
end
