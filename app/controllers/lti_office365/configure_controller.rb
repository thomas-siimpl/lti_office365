require_dependency 'lti_office365/application_controller'

module LtiOffice365
  class ConfigureController < ApplicationController
    def show
      respond_to do |format|
        format.xml do
          render xml: Rails.application.config.office365[:launch].to_xml
        end
      end
    end
  end
end
