require_dependency 'lti_office365/application_controller'

module LtiOffice365
  class SessionsController < ApplicationController
    def create
      @user = User.from_omniauth(auth_hash)
      session[:user_id] = @user.id
      flash[:success] = "Welcome, #{@user.name}!"
      redirect_to launch_path
    rescue
      flash[:warning] = 'Could not authenticate'
      redirect_to launch_path
    end

    def destroy
      if current_user
        session.delete(:user_id)
        flash[:success] = 'Successfully logged out'
      end
      redirect_to launch_path
    end

    protected

    def auth_hash
      request.env['omniauth.auth']
    end
  end
end
