module LtiOffice365
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception

    protected

    def allow_iframe
      response.headers['X-Frame-Options'] = 'ALLOWALL'
    end

    private

    def current_user
      @current_user ||= User.find_by(id: session[:user_id])
    end
    helper_method :current_user
  end
end
