require_dependency 'lti_office365/application_controller'

module LtiOffice365
  class LaunchController < ApplicationController
    skip_before_action :verify_authenticity_token
    before_action :allow_iframe, :post_params

    def show
    end

    protected

    def post_params
      req = request.request_parameters.merge(request.query_parameters)
      lti_message = IMS::LTI::Models::Messages::Message.generate(req)
      lti_message.launch_url = request.url
      params.merge!(message: lti_message.post_params)
    end
  end
end
