require 'test_helper'

class Office365::OnedriveServiceTest < ActionDispatch::IntegrationTest
  setup do
    @onedrive_id = '6746794741'
    @onedrive_file_url = 'https://graph.microsoft.com/path/to/my/file'
    @onedrive_endpoint = 'https://graph.microsoft.com/v1.0/me/drive/items'
    @onedrive_service = Office365::OnedriveService
    @onedrive_item_endpoint = "#{@onedrive_endpoint}/#{@onedrive_id}/content"
  end

  test '#create_share_link as authorized user and `onedrive_id`' do
    user = lti_office365_users(:one)
    stub_token_request(user)
    stub_onedrive_request
    service = @onedrive_service.new(user)
    url = service.create_share_link(@onedrive_id)
    assert_equal url, @onedrive_file_url
  end

  test '#create_share_link as unauthorized user and `onedrive_id`' do
    service = @onedrive_service.new(unauthorized_user)
    assert_raise AuthenticationException do
      service.create_share_link(@onedrive_id)
    end
  end

  private

  def token_request_body(user)
    {
      client_id: ENV['OFFICE365_CLIENT_ID'],
      client_secret: ENV['OFFICE365_CLIENT_SECRET'],
      grant_type: 'refresh_token',
      refresh_token: user.refresh_token,
      resource: Office365::GRAPH_RESOURCE
    }
  end

  def token_response_body
    {
      access_token: nil,
      refresh_token: nil
    }
  end

  def stub_onedrive_request
    stub_request(:get, @onedrive_item_endpoint).with(
      headers: { 'Authorization' => 'Bearer' }
    ).to_return(
      status: 200,
      headers: { 'Location' => @onedrive_file_url }
    )
  end

  def stub_token_request(user)
    stub_request(:post, Office365::TOKEN_ENDPOINT).with(
      body: token_request_body(user).to_query
    ).to_return(
      status: 200,
      body: token_response_body.to_json,
      headers: { 'Content-Type' => 'application/json' }
    )
  end

  # Nullify user token, go around NOT NULL constraint
  def unauthorized_user
    user = lti_office365_users(:two)
    user.token = nil
    user.save validate: false
    user
  end
end
