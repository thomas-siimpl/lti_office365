require 'test_helper'

class Office365::ClientTest < ActionDispatch::IntegrationTest
  setup do
    ENV['OFFICE365_CLIENT_ID'] = '7705465254'
    ENV['OFFICE365_CLIENT_SECRET'] = '7046527882'
    @graph_onedrive_endpoint = Office365::GRAPH_ONEDRIVE_ENDPOINT
    @client = Office365::Client
  end

  test '#get as an authorized user' do
    user = lti_office365_users(:one)
    stub_token_request(user)
    stub_action :get
    client = @client.new(user)
    api_response = client.get(@graph_onedrive_endpoint)
    assert_equal api_response.code, 200
  end

  test '#get as an unauthorized user' do
    client = @client.new(unauthorized_user)
    assert_raise AuthenticationException do
      client.get(@graph_onedrive_endpoint)
    end
  end

  test '#post as an authorized user' do
    user = lti_office365_users(:one)
    stub_token_request(user)
    stub_action :post
    client = @client.new(user)
    api_response = client.post(@graph_onedrive_endpoint)
    assert_equal api_response.code, 200
  end

  test '#post as an unauthorized user' do
    client = @client.new(unauthorized_user)
    assert_raise AuthenticationException do
      client.post(@graph_onedrive_endpoint)
    end
  end

  test '#put as an authorized user' do
    user = lti_office365_users(:one)
    stub_token_request(user)
    stub_action :put
    client = @client.new(user)
    api_response = client.put(@graph_onedrive_endpoint)
    assert_equal api_response.code, 200
  end

  test '#put as an unauthorized user' do
    client = @client.new(unauthorized_user)
    assert_raise AuthenticationException do
      client.put(@graph_onedrive_endpoint)
    end
  end

  test '#delete as an authorized user' do
    user = lti_office365_users(:one)
    stub_token_request(user)
    stub_action :delete
    client = @client.new(user)
    api_response = client.delete(@graph_onedrive_endpoint)
    assert_equal api_response.code, 200
  end

  test '#delete as an unauthorized user' do
    client = @client.new(unauthorized_user)
    assert_raise AuthenticationException do
      client.delete(@graph_onedrive_endpoint)
    end
  end

  private

  def token_request_body(user)
    {
      client_id: ENV['OFFICE365_CLIENT_ID'],
      client_secret: ENV['OFFICE365_CLIENT_SECRET'],
      grant_type: 'refresh_token',
      refresh_token: user.refresh_token,
      resource: Office365::GRAPH_RESOURCE
    }
  end

  def token_response_body
    {
      access_token: nil,
      refresh_token: nil
    }
  end

  def stub_action(method)
    stub_request(method, Office365::GRAPH_ONEDRIVE_ENDPOINT)
      .with(headers: { 'Authorization' => 'Bearer' })
      .to_return(status: 200, headers: {})
  end

  def stub_token_request(user)
    stub_request(:post, Office365::TOKEN_ENDPOINT).with(
      body: token_request_body(user).to_query
    ).to_return(
      status: 200,
      body: token_response_body.to_json,
      headers: { 'Content-Type' => 'application/json' }
    )
  end

  # Nullify user token, go around NOT NULL constraint
  def unauthorized_user
    user = lti_office365_users(:two)
    user.token = nil
    user.save validate: false
    user
  end
end
