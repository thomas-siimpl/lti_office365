require 'test_helper'

class Office365::CredentialServiceTest < ActionDispatch::IntegrationTest
  setup do
    @credential_service = Office365::CredentialService
  end

  test '#ensure_credentials as an authorized user' do
    user = lti_office365_users(:one)
    stub_token_request(user)
    @credential_service.ensure_credentials(user)
  end

  test '#ensure_credentials as an unauthorized user' do
    assert_raise AuthenticationException do
      @credential_service.ensure_credentials(unauthorized_user)
    end
  end

  private

  def token_request_body(user)
    {
      client_id: ENV['OFFICE365_CLIENT_ID'],
      client_secret: ENV['OFFICE365_CLIENT_SECRET'],
      grant_type: 'refresh_token',
      refresh_token: user.refresh_token,
      resource: Office365::GRAPH_RESOURCE
    }
  end

  def token_response_body
    {
      access_token: nil,
      refresh_token: nil
    }
  end

  def stub_token_request(user)
    stub_request(:post, Office365::TOKEN_ENDPOINT).with(
      body: token_request_body(user).to_query
    ).to_return(
      status: 200,
      body: token_response_body.to_json,
      headers: { 'Content-Type' => 'application/json' }
    )
  end

  # Nullify user token, go around NOT NULL constraint
  def unauthorized_user
    user = lti_office365_users(:two)
    user.token = nil
    user.save validate: false
    user
  end
end
