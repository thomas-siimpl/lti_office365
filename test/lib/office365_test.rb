require 'test_helper'

class Office365Test < ActionDispatch::IntegrationTest
  test 'endpoint generated correctly without item id' do
    endpoint = Office365.directory
    assert_equal endpoint, "#{Office365::GRAPH_ONEDRIVE_ENDPOINT}/root/children"
  end
  test 'endpoint generated correctly with item id' do
    id = '1779371448'
    endpoint = Office365.directory(id)
    assert_equal endpoint, "#{Office365::GRAPH_ONEDRIVE_ENDPOINT}/items/#{id}/children"
  end
end
