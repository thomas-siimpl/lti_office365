LtiOffice365::Engine.routes.draw do
  match '/launch', to: 'launch#show', via: [:get, :post]
  get '/configure', to: 'configure#show'
  get '/files(/:id)', to: 'files#index', as: 'directory'
  get '/files/download/:id', to: 'files#download', as: 'download'
  get '/auth/:provider/callback', to: 'sessions#create'
  match '/logout', to: 'sessions#destroy', via: [:get, :delete]
end
