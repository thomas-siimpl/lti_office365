module Office365
  class OnedriveService
    def initialize(user)
      @user = user
    end

    def create_share_link(onedrive_id)
      # https://graph.microsoft.com/v1.0/me/drive/items/#{item_id}/content
      endpoint = format('%s/items/%s/content', GRAPH_ONEDRIVE_ENDPOINT, onedrive_id)
      options = {
        headers: { 'Content-Type' => 'application/json' },
        follow_redirects: false
      }
      response = office365_client.get(endpoint, options)
      response.headers['location']
    end

    private

    def office365_client
      @office365_client ||= Client.new(@user)
    end
  end
end
