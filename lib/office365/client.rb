module Office365
  class Client
    def initialize(user)
      @user = user
    end

    %w(get post put delete).each do |method|
      define_method(method) do |endpoint, options = {}|
        action(method.to_sym, endpoint, options)
      end
    end

    protected

    def action(method, endpoint, options = {})
      ensure_credentials
      options.deep_merge! @auth_options
      HTTParty.send(method, endpoint, options)
    end

    def ensure_credentials
      CredentialService.ensure_credentials(@user)
      @auth_options = { headers: { 'Authorization' => "Bearer #{@user.token}" } }
    end
  end
end
