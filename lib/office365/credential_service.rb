module Office365
  class CredentialService
    class << self
      def ensure_credentials(user)
        raise AuthenticationException unless user.token.present?
        refresh_token(user)
      end

      private

      def refresh_token(user)
        response = HTTParty.post(TOKEN_ENDPOINT, body: refresh_request_body(user))
        raise AuthenticationException unless response.code == 200
        update_user_tokens(user, response)
      end

      def refresh_request_body(user)
        {
          client_id: ENV['OFFICE365_CLIENT_ID'],
          client_secret: ENV['OFFICE365_CLIENT_SECRET'],
          grant_type: 'refresh_token',
          refresh_token: user.refresh_token,
          resource: GRAPH_RESOURCE
        }
      end

      def update_user_tokens(user, response)
        raise AuthenticationException unless response.code == 200
        response = response.parsed_response
        user.update(
          token: response['access_token'],
          refresh_token: response['refresh_token']
        )
      end
    end
  end
end
