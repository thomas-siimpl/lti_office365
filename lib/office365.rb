# 401
class AuthenticationException < StandardError; end

module Office365
  GRAPH_RESOURCE = 'https://graph.microsoft.com/'.freeze
  GRAPH_ONEDRIVE_ENDPOINT = 'https://graph.microsoft.com/v1.0/me/drive'.freeze
  TOKEN_ENDPOINT = 'https://login.microsoftonline.com/common/oauth2/token'.freeze

  class << self
    def directory(id = nil)
      if id
        "#{GRAPH_ONEDRIVE_ENDPOINT}/items/#{id}/children"
      else
        "#{GRAPH_ONEDRIVE_ENDPOINT}/root/children"
      end
    end
  end
end

require 'office365/client'
require 'office365/credential_service'
require 'office365/onedrive_service'
