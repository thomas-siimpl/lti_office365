module LtiOffice365
  class Provider
    def initialize(strategy)
      @strategy = strategy
    end

    def client_id
      ENV['OFFICE365_CLIENT_ID']
    end

    def client_secret
      ENV['OFFICE365_CLIENT_SECRET']
    end
  end
end
