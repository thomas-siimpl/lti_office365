module LtiOffice365
  class Engine < ::Rails::Engine
    isolate_namespace LtiOffice365
    initializer 'lti_office365.config' do
      LtiOffice365::Config.setup
    end
    initializer 'lti_office365.config_provider' do |app|
      app.config.middleware.use OmniAuth::Builder do
        provider :azure_oauth2, LtiOffice365::Provider, name: :office365
      end
    end
  end
end
