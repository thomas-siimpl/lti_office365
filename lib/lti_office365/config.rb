module LtiOffice365
  module Config
    class << self
      def config_file
        root = LtiOffice365::Engine.root
        fpath = Rails.root.join('config', 'launch.yml')
        unless File.exist?(fpath)
          Rails.logger.warn 'Please create `config/launch.yml`'
          fpath = root.join('config', 'launch.example.yml')
        end
        fpath
      end

      def config_load
        YAML.load_file(config_file)[Rails.env]
      end

      def lti_launch
        IMS::LTI::Services::ToolConfig.new(config_load)
      end

      def configs_hash
        {
          launch: lti_launch
        }
      end

      def setup
        Rails.application.configure do
          config.office365 = LtiOffice365::Config.configs_hash
        end
      end
    end
  end
end
