# Quick Start

This document describes all of the steps needed to get a bare-bones LTI Tool Provider with Office365 up and running with the Canvas LMS from Instructure.  

See the `README.md` file for information on extending the functionality for different launch placements, and for different LTI Tool Consumers.

## Get the Project

```
git clone https://thomas-siimpl@bitbucket.org/thomas-siimpl/lti_office365.git
cd test/dummy
gem install bundler
bundle install
bundle exec rake lti_office365:install:migrations db:migrate
```

## Determine Deploy Location

The deploy must support the HTTPS protocol.  Some quick options:

 * Use [ngrok](https://ngrok.com) to forward a public URL to your local machine
 * [Generate a self-signed cert](https://msdn.microsoft.com/en-us/library/ms733813(v=vs.110).aspx) and [configure WEBrick to use it](http://stackoverflow.com/a/23283909/684934)

### Update LTI Config

Once your deploy location has been determined, you must change the LTI configuration to reflect the LTI launch URLs.

Edit the `config/launch.example.yml` and replace the `localhost` component with your actual deploy location.

## Set up Azure AD

You will need an Azure ActiveDirectory application registered with the following configuration:

 * Application is Multi-Tenant: `Yes`
 * Single sign-on
  * Reply URL: `<your deploy location>`
 * Permissions to other applications
  * Microsoft Graph: `Read user files`, `Sign in and read user profile`

Take the `Client ID` and app key, and set them in your environment variables:

* `OFFICE365_CLIENT_ID`
* `OFFICE365_CLIENT_SECRET`

(Re)Start your server at this time to pick up the credentials for Azure AD.

## Install Tool Provider into target Tool Consumer

* Navigate to `https://<deploy-url>/configuration.xml` and note the output.
* Sign into Canvas LMS as an administrator or teacher.
* `Settings` > `Apps` > `View App Configurations` > `Add App`
 * `Configuration Type` -> `Paste XML`
 * `Name` -> `Office365 LTI`
 * `Consumer key` and `Shared Secret` can be left blank.
 * `XML Configuration` -> paste in the XML from `/configuration.xml`

## Test the LTI

* Navigate to a course
* Click the `Office365 LTI` item in the navigation bar to the left.
* Launch the Office365 OAuth flow and sign in with your Microsoft for Business account.
* Observe your OneDrive folder contents are echoed.
